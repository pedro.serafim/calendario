import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class Main {
    public static void main(String[] args) throws ParseException {

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simple = new SimpleDateFormat(padrao);
        try {
            Date date = simple.parse("04/10/2019");
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(simple.format(new Date()));

        padrao = "dd/MM/yyyy HH:mm:ss";
        simple = new SimpleDateFormat(padrao);
        System.out.println(simple.format(new Date()));
    }
}

