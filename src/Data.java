import java.util.Calendar;
import java.util.Date;

public class Data {

    public Data(Date data){
        System.out.println(calculaIdade(data));
    }
    public Integer calculaIdade(Date dataNasc){
        Date hoje = new Date();
        Calendar calendario = Calendar.getInstance();

        calendario.setTime(hoje);
        Integer day1 = calendario.get(Calendar.DAY_OF_YEAR);
        Integer ano1 = calendario.get(Calendar.YEAR);

        calendario.setTime(dataNasc);
        Integer day2 = calendario.get(Calendar.DAY_OF_YEAR);
        Integer ano2 = calendario.get(Calendar.YEAR);

        Integer antesAno = ano1 - ano2;

        if(day1 < day2)
            antesAno--;

        return antesAno;
    }
}

